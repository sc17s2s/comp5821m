///////////////////////////////////////////////////
//
//  Hamish Carr
//  September, 2020
//
//  ------------------------
//  DirectedEdgeSurface.cpp
//  ------------------------
//
//  Base code for rendering assignments.
//
//  Minimalist (non-optimised) code for reading and
//  rendering an object file
//
//  We will make some hard assumptions about input file
//  quality. We will not check for manifoldness or
//  normal direction, &c.  And if it doesn't work on
//  all object files, that's fine.
//
//  While I could set it up to use QImage for textures,
//  I want this code to be reusable without Qt, so I
//  shall make a hard assumption that textures are in
//  ASCII PPM and use my own code to read them
//
///////////////////////////////////////////////////

// include the header file
#include "DirectedEdgeSurface.h"

// include the C++ standard libraries we want
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <math.h>

// include the Cartesian 3- vector class
#include "Cartesian3.h"
#include "SphereVertices.h"

#define MAXIMUM_LINE_LENGTH 1024

// constructor will initialise to safe values
DirectedEdgeSurface::DirectedEdgeSurface()
    : centreOfGravity(0.0,0.0,0.0)
    { // DirectedEdgeSurface()
    // force arrays to size 0
    vertices.resize(0);
    normals.resize(0);
	  firstDirectedEdge.resize(0);
	  faceVertices.resize(0);
	  otherHalf.resize(0);

    newVertices.resize(0);
    edges.resize(0);
    faces.resize(0);
    newEdges.resize(0);
    adjacentVertices.resize(0);
    neighbours.resize(0);
    } // DirectedEdgeSurface()

// read routine returns true on success, failure otherwise
bool DirectedEdgeSurface::ReadObjectStream(std::istream &geometryStream)
    { // ReadObjectStream()

    // create a read buffer
    char readBuffer[MAXIMUM_LINE_LENGTH];

    // the rest of this is a loop reading lines & adding them in appropriate places
    while (true)
        { // not eof
		// token for identifying meaning of line
		std::string token;

        // character to read
        geometryStream >> token;

        // check for eof() in case we've run out
        if (geometryStream.eof())
            break;

        // otherwise, switch on the token we read
		if (token == "#")
			{ // comment
			// read and discard the line
			geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
            } // comment
		else if (token == "Vertex")
			{ // vertex
			// variables for the read
			unsigned int vertexID;
			geometryStream >> vertexID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (vertexID != vertices.size())
				{ // bad vertex ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
				} // bad vertex ID

			// read in the new vertex position
			Cartesian3 newVertex;
			geometryStream >> newVertex;

			// and add it to the vertices
			vertices.push_back(newVertex);
			} // vertex
		else if (token == "Normal")
			{ // normal
			// variables for the read
			unsigned int normalID;
			geometryStream >> normalID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (normalID != normals.size())
				{ // bad ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
				} // bad ID

			// read in the new normal
			Cartesian3 newNormal;
			geometryStream >> newNormal;

			// and add it to the vertices
			normals.push_back(newNormal);
			} // normal
		else if (token == "FirstDirectedEdge")
			{ // first directed edge
			// variables for the read
			unsigned int FDEID;
			geometryStream >> FDEID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (FDEID != firstDirectedEdge.size())
				{ // bad ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
				} // bad ID

			// read in the new FDE
			unsigned int newFDE;
			geometryStream >> newFDE;

			// and add it to the vertices
			firstDirectedEdge.push_back(newFDE);
			} // first directed edge
		else if (token == "Face")
			{ // face
			// variables for the read
			unsigned int faceID;
			geometryStream >> faceID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (faceID != faceVertices.size()/3)
				{ // bad face ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
				} // bad face ID

			// read in the new face vertex (3 times)
			unsigned int newFaceVertex;
			geometryStream >> newFaceVertex;
			faceVertices.push_back(newFaceVertex);
			geometryStream >> newFaceVertex;
			faceVertices.push_back(newFaceVertex);
			geometryStream >> newFaceVertex;
			faceVertices.push_back(newFaceVertex);
			} // face
		else if (token == "OtherHalf")
			{ // other half
			// variables for the read
			unsigned int otherHalfID;
			geometryStream >> otherHalfID;
			// it has to be next valid 0-based ID, so
			// reject line if it isn't
			if (otherHalfID != otherHalf.size())
				{ // bad ID
				// read and discard the line
				geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
				} // bad ID

			// read in the new face vertex (3 times)
			unsigned int newOtherHalf;
			geometryStream >> newOtherHalf;
			otherHalf.push_back(newOtherHalf);
			} // other half
        } // not eof

    // compute centre of gravity
    // note that very large files may have numerical problems with this
    centreOfGravity = Cartesian3(0.0, 0.0, 0.0);

    // if there are any vertices at all
    if (vertices.size() != 0)
        { // non-empty vertex set
        // sum up all of the vertex positions
        for (unsigned int vertex = 0; vertex < vertices.size(); vertex++)
            centreOfGravity = centreOfGravity + vertices[vertex];

        // and divide through by the number to get the average position
        // also known as the barycentre
        centreOfGravity = centreOfGravity / vertices.size();

        // start with 0 radius
        objectSize = 0.0;

        // now compute the largest distance from the origin to a vertex
        for (unsigned int vertex = 0; vertex < vertices.size(); vertex++)
            { // per vertex
            // compute the distance from the barycentre
            float distance = (vertices[vertex] - centreOfGravity).length();

            // now test for maximality
            if (distance > objectSize)
                objectSize = distance;
            } // per vertex
        } // non-empty vertex set

    // return a success code
    return true;
    } // ReadObjectStream()

// write routine outputs to a new file
void DirectedEdgeSurface::WriteObjectStream(std::ostream &geometryStream) { // WriteObjectStream()
	geometryStream << "#" << std::endl;
	geometryStream << "# Created for Leeds COMP 5821M Autumn 2020" << std::endl;
	geometryStream << "#" << std::endl;
	geometryStream << "#" << std::endl;
	geometryStream << "# Surface vertices=" << newVertices.size() << " faces=" << faces[0].size()/3*4 << std::endl;
	geometryStream << "#" << std::endl;

	// output the vertices
  for (unsigned int vertex = 0; vertex < newVertices.size(); vertex++)
    geometryStream << "Vertex " << vertex << " " << std::fixed << newVertices[vertex] << std::endl;

  // and the normal vectors
  for (unsigned int normal = 0; normal < normals.size(); normal++)
    geometryStream << "Normal " << normal << " " << std::fixed << normals[normal] << std::endl;

	// and the first directed edges
  for (unsigned int vertex = 0; vertex < firstDirectedEdge.size(); vertex++)
    geometryStream << "FirstDirectedEdge " << vertex<< " " << std::fixed << firstDirectedEdge[vertex] << std::endl;

  // and the faces
  for (unsigned int face = 0; face < faceVertices.size(); face+=3) { // per original face
    for (int i = 0; i < 4; i++) { // per row
      geometryStream << "Face " << (face/3)*4 + i << " ";

      // output three vertices
      geometryStream << faces[i][face] << " ";
      geometryStream << faces[i][face+1] << " ";
      geometryStream << faces[i][face+2];

      geometryStream << std::endl;
    }
  }

	// and the other halves
	for (unsigned int dirEdge = 0; dirEdge < newEdges.size(); dirEdge++)
		geometryStream << "OtherHalf " << dirEdge << " " << newEdges[dirEdge].var << std::endl;

} // WriteObjectStream()

// main subdivision computations
void DirectedEdgeSurface::Subdivide() {
  // copy original vertices
  newVertices = vertices;
  // set size of 2D vector
  faces = std::vector<std::vector<unsigned int>>(4, std::vector<unsigned int> (faceVertices.size(), 0));

  // for each face, add original edges and edge vertices
  for (unsigned int f = 0; f < faceVertices.size(); f+=3) {

    //////////////vertex 0 to vertex 1//////////////
		// create a temporary edge
    tempEdge = AddEdge(faceVertices[f], faceVertices[f+1]);
    // check if OH has been created and has the edge vertex
    int midVertex = FindOHVertex(faceVertices[f], faceVertices[f+1]);

    // OH does not exist
    if (midVertex == -1) {
      // set the vertex ID of the edge vertex
      tempEdge.var = newVertices.size();
      // create edge vertex and add to newVertices
      AddVertex();

    // OH exists so use middle vertex
    } else {
      // set the ID of the edge vertex using the OH's
      tempEdge.var = midVertex;
    }
    // add edge to edges vector for original edges
    edges.push_back(tempEdge);

    // add the new vertex to the 1st row of new faces vector
    faces[0][f] = tempEdge.var;

    // repeat for the remaining edges
		//////////////vertex 1 to vertex 2//////////////
    tempEdge = AddEdge(faceVertices[f+1], faceVertices[f+2]);
    midVertex = FindOHVertex(faceVertices[f+1], faceVertices[f+2]);

    if (midVertex == -1) {
      tempEdge.var = newVertices.size();
      AddVertex();
    } else {
      tempEdge.var = midVertex;
    }

    edges.push_back(tempEdge);

    faces[0][f+1] = tempEdge.var;

    //////////////vertex 2 to vertex 0//////////////
    tempEdge = AddEdge(faceVertices[f+2], faceVertices[f]);
    midVertex = FindOHVertex(faceVertices[f+2], faceVertices[f]);

    if (midVertex == -1) {
      tempEdge.var = newVertices.size();
      AddVertex();
    } else {
      tempEdge.var = midVertex;
    }

    edges.push_back(tempEdge);

    faces[0][f+2] = tempEdge.var;
  }

  ////////////////////////// FACES AND EDGES //////////////////////////
  // except 1st row of faces which has already been done in the previous loop

  // for each original face
  for (unsigned int f = 0; f < faceVertices.size(); f+=3) {
    // subdivided faces
    // 2nd row
    faces[1][f] = faceVertices[f];
    faces[1][f+1] = faces[0][f];
    faces[1][f+2] = faces[0][f+2];
    // 3rd row
    faces[2][f] = faceVertices[f+1];
    faces[2][f+1] = faces[0][f+1];
    faces[2][f+2] = faces[0][f];
    // 4th row
    faces[3][f] = faceVertices[f+2];
    faces[3][f+1] = faces[0][f+2];
    faces[3][f+2] = faces[0][f+1];

    // subdivided edges
    for (int i = 0; i < 4; i++) { // per row in new faces vector
      //////////////vertex 0 to vertex 1//////////////
      // create a new edge
      tempEdge = AddEdge(faces[i][f], faces[i][f+1]);
      // find edge ID of OH
      int oh = FindOH(faces[i][f], faces[i][f+1]);
      // if OH exists
      if (oh != -1) {
        // set OH ID for both edges
        tempEdge.var = oh;
        newEdges[oh].var = newEdges.size();
      }
      // add edge to the new edges vector
      newEdges.push_back(tempEdge);

      // repeat for the remaining edges
      //////////////vertex 1 to vertex 2//////////////
      tempEdge = AddEdge(faces[i][f+1], faces[i][f+2]);
      oh = FindOH(faces[i][f+1], faces[i][f+2]);
      if (oh != -1) {
        tempEdge.var = oh;
        newEdges[oh].var = newEdges.size();
      }
      newEdges.push_back(tempEdge);

      //////////////vertex 2 to vertex 0//////////////
      tempEdge = AddEdge(faces[i][f+2], faces[i][f]);
      oh = FindOH(faces[i][f+2], faces[i][f]);
      if (oh != -1) {
        tempEdge.var = oh;
        newEdges[oh].var = newEdges.size();
      }
      newEdges.push_back(tempEdge);
    }
  }

  // compute all FDEs
  FDE();
  // calculate coordinates of all vertices
  NewVertexPositions();
}

// create and return a new edge object
struct DirectedEdgeSurface::Edge DirectedEdgeSurface::AddEdge(int v0, int v1) {
	Edge e;
	e.v0 = v0;
	e.v1 = v1;
  e.var = -1; // no OH or edge vertex set
	return e;
}

// return the vertex ID of the OH's edge vertex
int DirectedEdgeSurface::FindOHVertex(int v0, int v1) {
  for (unsigned int i = 0; i < edges.size(); i++) {
    if (edges[i].v0 == v1 && edges[i].v1 == v0) {
      return edges[i].var;
    }
  }
  // no OH found
  return -1;
}

// return the edge ID of the OH
int DirectedEdgeSurface::FindOH(int v0, int v1) {
  for (unsigned int i = 0; i < newEdges.size(); i++) {
    if (newEdges[i].v0 == v1 && newEdges[i].v1 == v0) {
      return i;
    }
  }
  // no OH found
  return -1;
}

// create a new vertex and add to newVertices vector
void DirectedEdgeSurface::AddVertex() {
  Cartesian3 vertex = Cartesian3(0,0,0);
  newVertices.push_back(vertex);
}

// compute all FDEs
void DirectedEdgeSurface::FDE() {
  // reset the same vector for original FDEs
  firstDirectedEdge.resize(0);
  // for each vertex including new vertices
  for (unsigned int v = 0; v < newVertices.size(); v++) {
    for (unsigned int e = 0; e < newEdges.size(); e++) {
      // edge to the vertex
      if (newEdges[e].v1 == v) {
        // add only the first edge found for each vertex
        firstDirectedEdge.push_back(e);
        break;
      }
    }
  }
}

// calculate the new coordinates of all vertices
void DirectedEdgeSurface::NewVertexPositions() {
  // set the size of the 2D vectors
  adjacentVertices = std::vector<std::vector<unsigned int>>(4);
  neighbours = std::vector<std::vector<unsigned int>>(vertices.size());
  int n;  // number of neighbours
  float alpha;
  Cartesian3 sum; // sum of the vectors of neighbouring vertices
  int count = 2;  // for adding the vertex IDs to both the 3rd and 4th rows

  /////////////////////////// NEW VERTICES ///////////////////////////
  // find vertices on the faces adjacent to the new vertex

  // vertices on the same edge
  // for each new vertex (not original)
  for (unsigned int v = vertices.size(); v < newVertices.size(); v++) {
    // find the original edge with the matching edge vertex
    for (unsigned int i = 0; i < edges.size(); i++) {
      // add both adjacent vertices on the edge to vector
      if (edges[i].var == v) {
        adjacentVertices[0].push_back(edges[i].v0);
        adjacentVertices[1].push_back(edges[i].v1);
        break;
      }
    }

    // vertices on the adjacent faces
    for (unsigned int f = 0; f < faceVertices.size(); f+=3) {
      // find combination where an original face is made up of the 2 vertices found previously
      // add the third vertex of that face
      //////////////vertex 0 and vertex 1//////////////
      if ((faceVertices[f]==adjacentVertices[0].back() && faceVertices[f+1]==adjacentVertices[1].back()) ||
      (faceVertices[f+1]==adjacentVertices[0].back() && faceVertices[f]==adjacentVertices[1].back())) {
        // add vertex 2
        adjacentVertices[count++].push_back(faceVertices[f+2]);
      //////////////vertex 1 and vertex 2//////////////
      } else if ((faceVertices[f+1]==adjacentVertices[0].back() && faceVertices[f+2]==adjacentVertices[1].back()) ||
      (faceVertices[f+2]==adjacentVertices[0].back() && faceVertices[f+1]==adjacentVertices[1].back())) {
        // add vertex 0
        adjacentVertices[count++].push_back(faceVertices[f]);
      //////////////vertex 2 and vertex 0//////////////
      } else if ((faceVertices[f]==adjacentVertices[0].back() && faceVertices[f+2]==adjacentVertices[1].back()) ||
      (faceVertices[f+2]==adjacentVertices[0].back() && faceVertices[f]==adjacentVertices[1].back())) {
        // add vertex 1
        adjacentVertices[count++].push_back(faceVertices[f+1]);
      }
    }
    // reset counter for each new vertex
    count = 2;
  }

  // calculate positions
  for (unsigned int a = 0; a < adjacentVertices[0].size(); a++) {
    newVertices[vertices.size()+a] = (newVertices[adjacentVertices[0][a]] + newVertices[adjacentVertices[1][a]]) * 0.375 + (newVertices[adjacentVertices[2][a]] + newVertices[adjacentVertices[3][a]]) * 0.125;
  }

  /////////////////////////// ORIGINAL VERTICES ///////////////////////////
  // for each original vertex
  for (unsigned int v = 0; v < vertices.size(); v++) {
    // find neighbouring vertices
    for (unsigned int i = 0; i < newEdges.size(); i++) {
      if (newEdges[i].v0 == v) {
        neighbours[v].push_back(newEdges[i].v1);
      }
    }

    // number of neighbours
    n = neighbours[v].size();
    // set alpha value
    if (n == 3) {
      alpha = 0.1875;
    } else {
      alpha = (0.625 - pow(0.375 + 0.25*cos((2*M_PI)/n), 2))/n;
    }

    // sum up all the neighbours
    sum = newVertices[neighbours[v][0]];
    for (unsigned int i = 1; i < n; i++) {
      sum = sum + newVertices[neighbours[v][i]];
    }

    // calculate new position
    newVertices[v] = (1 - n*alpha)*vertices[v] + alpha*(sum);
  }
}

// routine to render
void DirectedEdgeSurface::Render(RenderParameters *renderParameters)
    { // Render()
    // Ideally, we would apply a global transformation to the object, but sadly that breaks down
    // when we want to scale things, as unless we normalise the normal vectors, we end up affecting
    // the illumination.  Known solutions include:
    // 1.   Normalising the normal vectors
    // 2.   Explicitly dividing the normal vectors by the scale to balance
    // 3.   Scaling only the vertex position (slower, but safer)
    // 4.   Not allowing spatial zoom (note: sniper scopes are a modified projection matrix)
    //
    // Inside a game engine, zoom usually doesn't apply. Normalisation of normal vectors is expensive,
    // so we will choose option 2.

    // Scale defaults to the zoom setting
    float scale = renderParameters->zoomScale;
    scale /= objectSize;

    //  now scale everything
    glScalef(scale, scale, scale);

    // apply the translation to the centre of the object if requested
    glTranslatef(-centreOfGravity.x, -centreOfGravity.y, -centreOfGravity.z);

    // start rendering
    glBegin(GL_TRIANGLES);

	  // set colour for pick render - ignored for regular render
	  glColor3f(1.0, 1.0, 1.0);

    // loop through the faces
	for (unsigned int face = 0; face < faceVertices.size(); face +=3)
		{ // per face
		// if we want flat normals, compute them here
		if (renderParameters->useFlatNormals)
			{ // flat normals
			// find two vectors along edges of the triangle
			Cartesian3 pq = vertices[faceVertices[face+1]] - vertices[faceVertices[face]];
			Cartesian3 pr = vertices[faceVertices[face+2]] - vertices[faceVertices[face]];

			// take their cross product and normalise
			Cartesian3 faceNormal = pq.cross(pr).unit();

			// and use it to set the glNormal
			glNormal3f(faceNormal.x * scale, faceNormal.y * scale, faceNormal.z * scale);
			} // flat normals

		// we have made a HARD assumption that we have enough normals
		for (unsigned int vertex = face; vertex < face+3; vertex++)
			{ // per vertex

			// if we are using smooth normals
			if (!renderParameters->useFlatNormals)
				// set the normal vector
				glNormal3f
					(
					normals[faceVertices[vertex]].x * scale,
					normals[faceVertices[vertex]].y * scale,
					normals[faceVertices[vertex]].z * scale
					);

			// and set the vertex position
			glVertex3f
				(
				vertices[faceVertices[vertex]].x,
				vertices[faceVertices[vertex]].y,
				vertices[faceVertices[vertex]].z
				);

			} // per vertex

		} // per face

    // close off the triangles
    glEnd();

    // now we add a second loop to render the vertices if desired
    if (!renderParameters->showVertices)
    	return;

	glDisable(GL_LIGHTING);

	// loop through the vertices
	for (unsigned int vertex = 0; vertex < vertices.size(); vertex++)
		{ // per vertex
		// use modelview matrix (not most efficient solution, but quickest to code)
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glTranslatef(vertices[vertex].x, vertices[vertex].y, vertices[vertex].z);
		glScalef(0.1 * renderParameters->vertexSize, 0.1 * renderParameters->vertexSize, 0.1 * renderParameters->vertexSize);
		renderTriangulatedSphere();
		glPopMatrix();
		} // per vertex

    } // Render()

///////////////////////////////////////////////////
//
//  Hamish Carr
//  September, 2020
//
//  ------------------------
//  DirectedEdgeSurface.h
//  ------------------------
//
//  Base code for rendering assignments.
//
//  Minimalist (non-optimised) code for reading and
//  rendering an object file
//
//  We will make some hard assumptions about input file
//  quality. We will not check for manifoldness or
//  normal direction, &c.  And if it doesn't work on
//  all object files, that's fine.
//
//  While I could set it up to use QImage for textures,
//  I want this code to be reusable without Qt, so I
//  shall make a hard assumption that textures are in
//  ASCII PPM and use my own code to read them
//
///////////////////////////////////////////////////

// include guard for DirectedEdgeSurface
#ifndef _DIRECTED_EDGE_SURFACE_H
#define _DIRECTED_EDGE_SURFACE_H

// include the C++ standard libraries we need for the header
#include <vector>
#include <iostream>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

// include the unit with Cartesian 3-vectors
#include "Cartesian3.h"
// the render parameters
#include "RenderParameters.h"
// the image class for a texture
#include "RGBAImage.h"

class DirectedEdgeSurface
    { // class DirectedEdgeSurface
    public:

    // struct for edge containing its relevant vertex IDs
  	struct Edge {
  		int v0; // start vertex
  		int v1; // end vertex
      int var; // extra variable for middle vertex or OH
  	} tempEdge;

    // vector of vertices
    std::vector<Cartesian3> vertices;

    // vector of normals
    std::vector<Cartesian3> normals;

    // vector of original edges containing edge vertex IDs
    std::vector<Edge> edges;

	  // vector of first directed edge IDs
	  std::vector<unsigned int> firstDirectedEdge;

    // vector of faces - stored as a single long array
    std::vector<unsigned int> faceVertices;

	  // vector of other half edge IDs
	  std::vector<unsigned int> otherHalf;

    // centre of gravity - computed after reading
    Cartesian3 centreOfGravity;

    // size of object - i.e. radius of circumscribing sphere centred at centre of gravity
    float objectSize;

    ////////////////////// VECTORS FOR NEW SURFACES //////////////////////

    // vector of vertices containing old and new
    std::vector<Cartesian3> newVertices;

    // 2D vector of new faces - each column consists of new faces
    // making up an original face with the centre face in the top row
    std::vector<std::vector<unsigned int>> faces;

    // vector of subdivided edges including OH IDs
    std::vector<Edge> newEdges;

    // 2D vector of vertex IDs of vertices in the faces adjacent to the new vertices
    // each column is for separate new vertices with rows 1 & 2 containing vertices on the same edge
    std::vector<std::vector<unsigned int>> adjacentVertices;

    // 2D vectors of vertex IDs of the neighbours of original vertices - each row is for a separate
    // original vertex and can be different lengths depending on the no. of neighbours
    std::vector<std::vector<unsigned int>> neighbours;

    // constructor will initialise to safe values
    DirectedEdgeSurface();

    // read routine returns true on success, failure otherwise
    bool ReadObjectStream(std::istream &geometryStream);

    // write routine outputs to a new file
    void WriteObjectStream(std::ostream &geometryStream);

    // main subdivision computations
    void Subdivide();

    // returns a new edge object
    struct Edge AddEdge(int v0, int v1);

    // returns the vertex ID of the OH's edge vertex
    int FindOHVertex(int v0, int v1);

    // returns the edge ID of the OH
    int FindOH(int v0, int v1);

    // creates a new vertex and adds to newVertices vector
    void AddVertex();

    // computes all FDEs
    void FDE();

    // calculates the new coordinates of all vertices
    void NewVertexPositions();

    // routine to render
    void Render(RenderParameters *renderParameters);

    }; // class DirectedEdgeSurface

// end of include guard for DirectedEdgeSurface
#endif

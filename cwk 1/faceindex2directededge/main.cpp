#include <stdio.h>
#include <iostream>
#include <fstream>
#include "FaceIndex2DirectedEdge.h"

int main(int argc, char **argv) {
	// create an object
	FaceIndex2DirectedEdge de;

	// check the args to make sure there's an input file
	if (argc == 2) { // two parameters - read a file
		// read failed
		if (!de.ReadFile(argv[1])) {
			printf("Read failed for file %s\n", argv[1]);

		// read succeeded
		} else {
			// compute edges, FDEs and OHs
			de.Edges();

			// check if the shape is manifold
			if (de.Manifold()) {
				// calculate the genus
				de.Genus();
				// write out to .diredge file
				de.WriteFile(argv[1]);

				std::cout << "It is manifold" << '\n';
			// not manifold
			} else {
				std::cout << "It is not manifold" << '\n';
			}
		}
	} else { // too many or no parameters
		printf("Usage: %s filename\n", argv[0]);
	}
	return 0;
}

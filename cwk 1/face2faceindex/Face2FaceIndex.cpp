#include "Face2FaceIndex.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>

// constructor
Face2FaceIndex::Face2FaceIndex() {
	vertices.resize(0);
	nTriangles = 0;
}

// create a .face file and output the information
void Face2FaceIndex::WriteFile(char *name) {
	// change .tri to .face
	std::string fileName = name;
	int pos = fileName.size() - 3;		// get the position before the extension
	fileName.replace(pos, 4, "face");

	// produce .face file
	std::ostringstream outputFile;
	outputFile << fileName;

	// output to file
	std::ofstream out(outputFile.str().c_str());

	// get only the name of the shape
	pos = fileName.find("models/");
	std::string shapeName = fileName.substr(pos+7);
	int length = shapeName.size() - 5;

	// header
	out << "# University of Leeds 2020-2021\n# COMP 5821M Assignment 1\n# Sulagna Sinha\n# 201098132\n#\n";
	out << "# Object Name: " << shapeName.substr(0, length) << "\n";
	out << "# Vertices=" << vertices.size() << " Faces=" << nTriangles << "\n#\n";

	// vertex block
	for (unsigned int i = 0; i < vertices.size(); i++) {
		out << "Vertex " << i << "  " << vertices[i] << '\n';
	}

	// face block
	for (unsigned int i = 0; i < faces.size(); i++) {
		out << "Face " << i << "  " << faces[i].v0 << "  " << faces[i].v1 << "  " << faces[i].v2 << '\n';
	}

	// close file
	out.close();
}

// read the file provided in the argument
bool Face2FaceIndex::ReadFile(char *fileName) {
	// could not read file
	std::ifstream inFile(fileName);
	if (!inFile.good())
		return false;

	// set the number of vertices
	long nVertices = 0;
	// determine which number vertex of a face
	int count = 0;

	// read in the number of triangles and set the number of vertices
	inFile >> nTriangles;
	nVertices = nTriangles * 3;

	// read in vertices and store coordinate information
	for (int v = 0; v < nVertices; v++) {
		inFile >> tempVertex.x >> tempVertex.y >> tempVertex.z;

		// add vertex to list if it doesn't exist
		if (!VertexExists(tempVertex)) {
			vertices.push_back(tempVertex);
		}

		// add vertex to a face depending on whether it is the 1st, 2nd or 3rd vertex
		if (count == 0) {
			tempFace.v0 = VertexIndex(tempVertex);
			count++;
		} else if (count == 1) {
			tempFace.v1 = VertexIndex(tempVertex);
			count++;
		} else if (count == 2) {
			tempFace.v2 = VertexIndex(tempVertex);

			// add face to vector
			faces.push_back(tempFace);
			count = 0;	// reset count
		}
	}

	// close file and read successful
	inFile.close();
	return true;
}

// check if the specified vertex is in the vertices vector
bool Face2FaceIndex::VertexExists(Cartesian3 vertex) {
	for (unsigned int i = 0; i < vertices.size(); i++) {
		if (vertex ==(vertices[i])) {
			return true;
		}
	}
	return false;
}

// return the index in the vertices vector of a vertex, -1 if it doesn't exist
int Face2FaceIndex::VertexIndex(Cartesian3 vertex) {
	for (unsigned int i = 0; i < vertices.size(); i++) {
		if (vertex ==(vertices[i])) {
			return i;
		}
	}
	return -1;
}

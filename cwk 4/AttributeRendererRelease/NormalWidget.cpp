//////////////////////////////////////////////////////////////////////
//
//  University of Leeds
//  COMP 5812M Foundations of Modelling & Rendering
//  User Interface for Coursework
//
//  September, 2020
//
//  -----------------------------
//  Normal Widget
//  -----------------------------
//
//  This widget primarily sets up the transformation matrices and
//  lighting for rendering the normal map
//
////////////////////////////////////////////////////////////////////////

#include <math.h>

// include the header file
#include "NormalWidget.h"

// constructor
NormalWidget::NormalWidget
        (
        // the geometric object to show
        AttributedObject 	*newAttributedObject,
        // the render parameters to use
        RenderParameters    *newRenderParameters,
        // parent widget in visual hierarchy
        QWidget             *parent
        )
    // the : indicates variable instantiation rather than arbitrary code
    // it is considered good style to use it where possible
    :
    // start by calling inherited constructor with parent widget's pointer
    QOpenGLWidget(parent),
    // then store the pointers that were passed in
    attributedObject(newAttributedObject),
    renderParameters(newRenderParameters)
    { // constructor
    // leaves nothing to put into the constructor body
    } // constructor

// destructor
NormalWidget::~NormalWidget()
    { // destructor
    // empty (for now)
    // all of our pointers are to data owned by another class
    // so we have no responsibility for destruction
    // and OpenGL cleanup is taken care of by Qt
    } // destructor

// called when OpenGL context is set up
void NormalWidget::initializeGL()
    { // NormalWidget::initializeGL()
    // set lighting parameters (may be reset later)
    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);

    // background is black
    glClearColor(0.0, 0.0, 0.0, 1.0);

    // enable depth-buffering
	glEnable(GL_DEPTH_TEST);
    } // NormalWidget::initializeGL()

// called every time the widget is resized
void NormalWidget::resizeGL(int w, int h)
    { // NormalWidget::resizeGL()
    // reset the viewport
    glViewport(0, 0, w, h);
    std::cout << w << " " << h << '\n';

    // set projection matrix to be glOrtho based on zoom & window size
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // compute the aspect ratio of the widget
    float aspectRatio = (float) w / (float) h;

    // we want to capture a sphere of radius 1.0 without distortion
    // so we set the ortho projection based on whether the window is portrait (> 1.0) or landscape
    // portrait ratio is wider, so make bottom & top -1.0 & 1.0
    if (aspectRatio > 1.0)
        glOrtho(-aspectRatio, aspectRatio, -1.0, 1.0, -1.0, 1.0);
    // otherwise, make left & right -1.0 & 1.0
    else
        glOrtho(-1.0, 1.0, -1.0/aspectRatio, 1.0/aspectRatio, -1.0, 1.0);

    } // NormalWidget::resizeGL()

// called every time the widget needs painting
void NormalWidget::paintGL()
    { // NormalWidget::paintGL()
    // clear the buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set model view matrix based on stored translation, rotation &c.
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // start with lighting turned off
    glDisable(GL_LIGHTING);

	// translate by the visual translation
	glTranslatef(renderParameters->xTranslate, renderParameters->yTranslate, 0.0f);

	// apply rotation matrix from arcball
	glMultMatrixf(renderParameters->rotationMatrix.columnMajor().coordinates);

    // tell the object to draw the normal map,
    // passing in the render parameters for reference
    attributedObject->RenderNormal(renderParameters);

    } // NormalWidget::paintGL()

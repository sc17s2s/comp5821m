# University of Leeds 2020-2021
# COMP 5821M Assignment 4 Texture & Normal Map Synthesis


To compile on the University Linux machines, you will need to do the following:
$ make

To execute the program, pass the file name on the command line:
$ ./AttributeRendererRelease <filename>

The PPM files will be created in the current directory.

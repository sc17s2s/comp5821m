#include <vector>
#include <string>
#include "Cartesian3.h"

// FaceIndex2DirectedEdge class
class FaceIndex2DirectedEdge {
	public:

	// struct for face containing its vertex and edge IDs
	struct Face {
		int v0;
		int v1;
		int v2;
		int e0;
		int e1;
		int e2;
	} tempFace;

	// struct for edge containing its starting and ending vertex IDs
	struct Edge {
		int v0;
		int v1;
	} tempEdge;

	// struct for first directed edge with vertex and edge IDs
	struct FDE {
		int vertex;
		int edge;
	};

	// struct containing the ID of an edge and its other half
	struct OH {
		int edge0;
		int edge1;
	};

	// vectors to store vertex, face, edge, FDE and other half information
	std::vector<Cartesian3> vertices;
	std::vector<Face> faces;
	std::vector<Edge> edges;
	std::vector<FDE> fde;
	std::vector<OH> otherHalf;

	// temporary vertex
	Cartesian3 tempVertex;

	// reads and stores the header from .face file
	std::string header;

	// constructor will initialise to safe values
	FaceIndex2DirectedEdge();
	// writes out to a .diredge file
	void WriteFile(char *name);
	// read routine returns true on success, failure otherwise
	bool ReadFile(char *fileName);
	// computes edges
	void Edges();
	// creates an edge
	struct Edge AddEdge(int v0, int v1);
	// finds the index of an edge in the edges vector
	int EdgeIndex(Edge e);
	// checks and creates other half
	void OtherHalf(int v0, int v1);
	// sorts the other half vector into ascending order by first edge
	void SortOH();
	// check if an edge exists from the vertex specified
	bool EdgeExistsFrom(int vertex);
	// check if shape is manifold
	bool Manifold();
	// check if all edges are unique
	bool UniqueEdges();
	// check if all edges have an other half
	bool HasOH();
	// check if any vertex is a pinch point
	bool PinchPoint();
	// compute the genus
	void Genus();

};

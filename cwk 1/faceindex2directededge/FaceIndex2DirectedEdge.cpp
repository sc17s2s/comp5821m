#include "FaceIndex2DirectedEdge.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

// constructor
FaceIndex2DirectedEdge::FaceIndex2DirectedEdge() {
	vertices.resize(0);
	faces.resize(0);
	edges.resize(0);
	fde.resize(0);
	otherHalf.resize(0);
}

// create a .diredge file and output the information
void FaceIndex2DirectedEdge::WriteFile(char *name) {
	// change .face to .diredge
	std::string fileName = name;
	int pos = fileName.size() - 4;		// get the position before the extension
	fileName.replace(pos, 7, "diredge");

	// produce .diredge file
	std::ostringstream outputFile;
	outputFile << fileName;

	// output to file
	std::ofstream out(outputFile.str().c_str());

	// header
	out << header;

	// vertex block
	for (unsigned int i = 0; i < vertices.size(); i++) {
		out << "Vertex " << i << "  " << vertices[i] << '\n';
	}

	// FDE block
	for (unsigned int i = 0; i < fde.size(); i++) {
		out << "FirstDirectedEdge " << fde[i].vertex << "  " << fde[i].edge << '\n';
	}

	// face block
	for (unsigned int i = 0; i < faces.size(); i++) {
		out << "Face " << i << "  " << faces[i].v0 << "  " << faces[i].v1 << "  " << faces[i].v2 << '\n';
	}

	// other half block
	for (unsigned int i = 0; i < otherHalf.size(); i++) {
		out << "OtherHalf " << otherHalf[i].edge0 << "  " << otherHalf[i].edge1 << '\n';
	}

	// close file
	out.close();
}

// read the file provided in the argument
bool FaceIndex2DirectedEdge::ReadFile(char *fileName) {
	// could not read file
	std::ifstream inFile(fileName);
	if (!inFile.good())
		return false;

	// variable to store read lines
	std::string text;

	// get 8 lines of the header
	for (int i = 0; i < 8; i++) {
		getline(inFile, text);
		header.append(text);
		header.append("\n");
	}

	// use header to find total number of vertices and faces
	int vpos = header.find("Vertices=");
	int fpos = header.find("Faces=");
	// length of the numbers in the string
	int fsize = header.size()-fpos-8;
	int vsize = header.size()-vpos-9-9-fsize;
	// create strings with the number of vertices and faces
	std::string v = header.substr(vpos+9, vsize);
	std::string f = header.substr(fpos+6, fsize);

	// convert number of vertices and faces to long ints
	long nVertices = std::stol(v);
	long nTriangles = std::stol(f);

	// read vertex block
	for (int v = 0; v < nVertices; v++) {
		inFile >> text;
		// check if the line starts with the Vertex keyword
		if (text == "Vertex") {
			// store the coordinates and add vertex to the list
			inFile >> text >> tempVertex.x >> tempVertex.y >> tempVertex.z;
			vertices.push_back(tempVertex);

		// error if line does not start with Vertex
		} else {
			std::cerr << "Error reading vertices" << '\n';
			return false;
		}
	}

	// read face block
	for (int f = 0; f < nTriangles; f++) {
		inFile >> text;
		// check if the line starts with the Face keyword
		if (text == "Face") {
			// store the vertex IDs and add face to the list
			inFile >> text >> tempFace.v0 >> tempFace.v1 >> tempFace.v2;
			faces.push_back(tempFace);

		// error if line does not start with Face
		} else {
			std::cerr << "Error reading faces" << '\n';
			return false;
		}
	}

	// close file and read successful
	inFile.close();
	return true;
}

// create edges and calculate FDEs and OHs
void FaceIndex2DirectedEdge::Edges() {
	FDE f;
	// compute all 3 edges of each face and their OH, and the FDEs
	for (unsigned int i = 0; i < faces.size(); i++) {
		//////////////vertex 0 to vertex 1//////////////
		// create a temporary edge
		tempEdge = AddEdge(faces[i].v0, faces[i].v1);

		// check if an edge exists from v0 to add this edge as its FDE
		if (!EdgeExistsFrom(faces[i].v0)) {
			// add temporary edge to list of edges and faces
			edges.push_back(tempEdge);
			faces[i].e0 = EdgeIndex(tempEdge);

			// create FDE and add to list
			f.vertex = faces[i].v0;
			f.edge = faces[i].e0;
			fde.push_back(f);

		// if an edge already exists, only add to list of edges and faces
		} else {
			edges.push_back(tempEdge);
			faces[i].e0 = EdgeIndex(tempEdge);
		}

		// check if edge exists from v1 to v0
		OtherHalf(faces[i].v0, faces[i].v1);

		// repeat for the remaining edges
		//////////////vertex 1 to vertex 2//////////////
		tempEdge = AddEdge(faces[i].v1, faces[i].v2);

		if (!EdgeExistsFrom(faces[i].v1)) {
			edges.push_back(tempEdge);
			faces[i].e1 = EdgeIndex(tempEdge);

			f.vertex = faces[i].v1;
			f.edge = faces[i].e1;
			fde.push_back(f);

		} else {
			edges.push_back(tempEdge);
			faces[i].e1 = EdgeIndex(tempEdge);
		}

		OtherHalf(faces[i].v1, faces[i].v2);

		//////////////vertex 2 to vertex 0//////////////
		tempEdge = AddEdge(faces[i].v2, faces[i].v0);

		if (!EdgeExistsFrom(faces[i].v2)) {
			edges.push_back(tempEdge);
			faces[i].e2 = EdgeIndex(tempEdge);

			f.vertex = faces[i].v2;
			f.edge = faces[i].e2;
			fde.push_back(f);

		} else {
			edges.push_back(tempEdge);
			faces[i].e2 = EdgeIndex(tempEdge);
		}

		OtherHalf(faces[i].v2, faces[i].v0);
	}

	//sort order of other half vector
	SortOH();
}

// create and return an edge
struct FaceIndex2DirectedEdge::Edge FaceIndex2DirectedEdge::AddEdge(int v0, int v1) {
	Edge e;
	e.v0 = v0;
	e.v1 = v1;
	return e;
}

// find and return the vector index of a specific edge, -1 if not found
int FaceIndex2DirectedEdge::EdgeIndex(Edge e) {
	for (unsigned int i = 0; i < edges.size(); i++) {
		if (e.v0 == edges[i].v0 && e.v1 == edges[i].v1) {
			return i;
		}
	}
	return -1;
}

// search the edges vector to check if any edges exist from a specific vertex
bool FaceIndex2DirectedEdge::EdgeExistsFrom(int vertex) {
	for (unsigned int i = 0; i < edges.size(); i++) {
		if (vertex == edges[i].v0) {
			return true;
		}
	}
	return false;
}

// search edges vector to find the other half of an edge and add to otherHalf vector
void FaceIndex2DirectedEdge::OtherHalf(int v0, int v1) {
	OH oh;
	for (unsigned int i = 0; i < edges.size(); i++) {
		// create an OH object if the other half is found
		if (edges[i].v0 == v1 && edges[i].v1 == v0) {
			oh.edge0 = i;
			oh.edge1 = EdgeIndex(AddEdge(v0, v1));
			otherHalf.push_back(oh);
			// add both edges as the other half of the other edge
			oh.edge0 = oh.edge1;
			oh.edge1 = i;
			otherHalf.push_back(oh);
		}
	}
}

// sort the OH objects in the other half vector into ascending order by edge0
void FaceIndex2DirectedEdge::SortOH() {
	std::sort(otherHalf.begin(), otherHalf.end(), [](OH const &i, OH const &j) {
		return (i.edge0 < j.edge0);
	});
}

// check if the shape is manifold - all edges share excatly 2 faces and no pinch points
bool FaceIndex2DirectedEdge::Manifold() {
	// check that all edges are unique and have an other half - share exactly 2 faces
	if (!UniqueEdges() || !HasOH()) {
		return false;
	}

	// check that there are no pinch points
	if (PinchPoint()) {
		return false;
	}

	// all criteria met for manifold
	return true;
}

// search edges for any repeats
bool FaceIndex2DirectedEdge::UniqueEdges() {
	for (unsigned int i = 0; i < edges.size(); i++) {
		for (unsigned int j = 0; j < edges.size(); j++) {
			if (i != j) {	// ignore cases where it is the same edge
				// found repeated edge
				if (edges[i].v0 == edges[j].v0 && edges[i].v1 == edges[j].v1) {
					std::cout << "Bad edge: " << j << " is a repeated edge" << '\n';
					return false;
				}
			}
		}
	}
	// no repeated edges found
	return true;
}

// check if all edges have an other half
bool FaceIndex2DirectedEdge::HasOH() {
	// flag to determine if OH was found
	int foundOH = 0;

	// search otherHalf vector for all edges
	for (unsigned int i = 0; i < edges.size(); i++) {
		for (unsigned int j = 0; j < otherHalf.size(); j++) {
			// change flag if found and move to next edge
			if (i == otherHalf[j].edge0) {
				foundOH = 1;
				break;
			}
		}

		// return false if any edge found to not have an OH
		if (foundOH == 0) {
			std::cout << "Bad edge: " << i << " does not have other half" << '\n';
			return false;
		}

		// reset flag
		foundOH = 0;
	}

	// OH found for all edges
	return true;
}

// check if any vertices are a pinch point
bool FaceIndex2DirectedEdge::PinchPoint() {
	int e, lastEdge = -1;		// lastEdge used to find of all edges from the vertex

	// store all visited edges from the vertex
	std::vector<int> visitedEdges;

	// cycle through all edges from each vertex by finding the previous edge of FDE and then OH
	for (unsigned int v = 0; v < vertices.size(); v++) {
		// reset vector
		visitedEdges.resize(0);
		// FDE of the vertex
		e = fde[v].edge;

		// find the previous edge and OH, then repeat until back to the FDE
		while (lastEdge != fde[v].edge) {
			// find previous edge
			for (unsigned int f = 0; f < faces.size(); f++) {
				if (faces[f].e0 == e) {
					e = faces[f].e2;
					break;
				} else if (faces[f].e1 == e) {
					e = faces[f].e0;
					break;
				} else if (faces[f].e2 == e) {
					e = faces[f].e1;
					break;
				}
			}

			// find other half
			for (unsigned int oh = 0; oh < otherHalf.size(); oh++) {
				if (otherHalf[oh].edge0 == e) {
					// OH is an edge from the vertex so add to visitedEdges
					lastEdge = otherHalf[oh].edge1;
					visitedEdges.push_back(lastEdge);

					e = lastEdge;
					break;
				}
			}
		}

		// check if all edges from the vertex were visited
		for (unsigned int i = 0; i < edges.size(); i++) {
			if (edges[i].v0 == v) {		// edge is from v
				// if edge is not in visitedEdges, return true for pinch point
				if (find(visitedEdges.begin(), visitedEdges.end(), i) == visitedEdges.end()) {
					std::cout << "Not visited edge " << i << '\n';
					std::cout << "Bad vertex: " << v << '\n';
					return true;
				}
			}
		}
	}
	// all edges from each vertex visited
	return false;
}

// calculate the genus
void FaceIndex2DirectedEdge::Genus() {
	// using Euler's formula
	int g = (2 - (vertices.size() - edges.size()/2 + faces.size()))/2;
	// exit if genus is negative
	if (g < 0) {
		std::cout << "It is not manifold - separate shapes" << '\n';
		exit(EXIT_FAILURE);
	}
	std::cout << "Genus: " << g << '\n';
}

#include <vector>
#include "Cartesian3.h"

// Face2FaceIndex class
class Face2FaceIndex {
	public:

	// struct for face containing its vertex IDs
	struct Face {
		int v0;
		int v1;
		int v2;
	} tempFace;

	// vectors to store vertex and triangle information
	std::vector<Cartesian3> vertices;
	std::vector<Face> faces;

	long nTriangles;

	// temporary vertex
	Cartesian3 tempVertex;

	// constructor will initialise to safe values
	Face2FaceIndex();
	// writes out to a .face file
	void WriteFile(char *name);
	// read routine returns true on success, failure otherwise
	bool ReadFile(char *fileName);
	// check if vertices vector contains a particular vertex
	bool VertexExists(Cartesian3 vertex);
	// return the index of a vertex in the vector
	int VertexIndex(Cartesian3 vertex);

};

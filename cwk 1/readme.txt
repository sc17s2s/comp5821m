# University of Leeds 2020-2021
# COMP 5821M Assignment 1 Data Structures

Two directories have been provided: face2faceindex and faceindex2directededge.
For part a) the face2faceindex directory is needed and the rest are in faceindex2directededge.
Any files created will be in the models directory along with the .tri files.


To compile on the University Linux machines, you will need to do the following:
$ make

You should see two compiler warnings about a comparison between signed and unsigned integer expressions, which can be ignored.

To execute the programs, pass the file name on the command line:
$ ./face2faceindex <filename>


Note: Only .tri files should be passed to face2faceindex and .face files to faceindex2directededge.
Large files can take some time to execute so please be patient.

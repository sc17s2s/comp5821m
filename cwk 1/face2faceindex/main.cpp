#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "Face2FaceIndex.h"

int main(int argc, char **argv) {
	// create an object
	Face2FaceIndex f;

	// check the args to make sure there's an input file
	if (argc == 2) { // two parameters - read a file
		// read failed
		if (!f.ReadFile(argv[1])) {
			printf("Read failed for file %s\n", argv[1]);

		// read succeeded
		} else {
			// write out to .face file
			f.WriteFile(argv[1]);
		}
	} else { // too many or no parameters
		printf("Usage: %s filename\n", argv[0]);
	}
	return 0;
}

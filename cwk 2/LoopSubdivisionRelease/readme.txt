# University of Leeds 2020-2021
# COMP 5821M Assignment 2 Mesh Operators & Loop Subdivision Surfaces


To compile on the University Linux machines, you will need to do the following:
$ make

To execute the program, pass the file name on the command line:
$ ./LoopSubdivisionRelease <filename>

Any .diredgenormal files created will be in the diredgenormals directory along with the other .diredgenormal files. Output files will have the same name as the input files but with a "1" at the end.

